FROM maven:3.6.0-jdk-11-slim AS build
COPY pom.xml .
RUN mvn clean  dependency:resolve -Dmaven.artifact.threads=4
# compilamos el codigo
COPY ./src src/
RUN mvn install -Dmaven.artifact.threads=4

FROM java:alpine
RUN mkdir /challenge
WORKDIR /challenge
COPY --from=0 target/challenge-0.0.1-SNAPSHOT.jar .
RUN apk add --no-cache tzdata
ENV TZ="America/Argentina/Buenos_Aires"

EXPOSE 8080