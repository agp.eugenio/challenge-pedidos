package com.challenge.controller;

import com.challenge.cache.CacheEventLogger;
import com.challenge.entity.Order;
import com.challenge.service.OrderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import java.net.URI;
import java.util.NoSuchElementException;

@RestController
@RequestMapping("order")
public class OrderController {
    static final String ORDER_NOT_FOUND = "That order does not exist";

    @Autowired
    private OrderService orderService;

    @PostMapping("")
    public ResponseEntity save(@RequestBody Order order) {
        Order createdOrder = orderService.save(order);
        return ResponseEntity.created(getResourceLocation(createdOrder.getId())).body(createdOrder);
    }

    @PutMapping("")
    public ResponseEntity update(@RequestBody Order order) {
        try {
            return ResponseEntity.ok(orderService.update(order));
        } catch (NoSuchElementException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(ORDER_NOT_FOUND);
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity getById(@PathVariable Long id) {
        try {
            return ResponseEntity.ok(orderService.getById(id));
        } catch (NoSuchElementException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(ORDER_NOT_FOUND);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable Long id) {
        orderService.delete(id);
        return ResponseEntity.noContent().build();
    }

    private URI getResourceLocation(Long id) {
        return ServletUriComponentsBuilder
                .fromCurrentRequest().path("/{id}")
                .buildAndExpand(id).toUri();
    }
}
