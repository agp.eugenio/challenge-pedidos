package com.challenge.service;

import com.challenge.entity.Order;
import com.challenge.repository.OrderRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;
import java.util.NoSuchElementException;
import java.util.Optional;

@Component
public class OrderServiceImpl implements OrderService{
    @Autowired
    OrderRepository orderRepository;

    @Override
    public Order save(Order order) {
        return orderRepository.save(order);
    }

    @Override
    @CachePut(cacheNames = "orderCache", key = "#order.id")
    public Order update(Order order) {
        Order previousOrder = getById(order.getId());
        BeanUtils.copyProperties(order, previousOrder);
        return orderRepository.save(previousOrder);
    }

    @Override
    @Cacheable(value = "orderCache", key = "#orderId")
    public Order getById(Long orderId) {
        Optional<Order> order = orderRepository.findById(orderId);
        if (order.isPresent()) {
            return order.get();
        } else {
            throw new NoSuchElementException("That order does not exist");
        }
    }

    @Override
    @CacheEvict(value = "orderCache", key = "#orderId")
    public void delete(Long orderId) {
        orderRepository.deleteById(orderId);
    }
}
