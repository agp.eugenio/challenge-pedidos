package com.challenge.service;

import com.challenge.entity.Order;

public interface OrderService {

    Order save(Order order);

    Order update(Order order);

    Order getById(Long orderId);

    void delete(Long orderId);
}
