# challenge-pedidos

# Swagger

http://localhost:8080/swagger-ui.html

# Para levantar

Tener levantado en local una base mysql en 3306, configurar user pass en props, correr script sql que esta en /resources

- Con java: java -jar challenge-0.0.1-SNAPSHOT.jar (Jar que adjunto al email)

- Con docker: docker-compose up --build en carpeta raíz

Tecnologías utilizadas:

Spring boot, spring jpa, ehcache, hibernate, mysql, docker